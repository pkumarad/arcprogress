package com.cumulations;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class ClientActivity extends Activity{

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	setContentView(R.layout.activity_main);
	
	ArcProgressBar prBar=(ArcProgressBar)findViewById(R.id.progressArc);	
	prBar.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	
	ArrayList<String> mainArrayList=new ArrayList<String>();
	mainArrayList.add("0");
	mainArrayList.add("1");
	mainArrayList.add("2");
	mainArrayList.add("3");
	
	
	ArrayList<String> smallArrayList=new ArrayList<String>();
	smallArrayList.add("<10");
	smallArrayList.add("<20");
	smallArrayList.add("<30");
	smallArrayList.add("<40");
	
	prBar.setMainTextArray(mainArrayList);	
	prBar.setSmallTextArray(smallArrayList);	
	
	
		
	}
}
